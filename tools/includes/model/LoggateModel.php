<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoggateModel extends CI_Model {

  protected $table;

  function __construct(){
      parent::__construct();
      $this->table = 'loggate';
  }

	public function insert($data){
		$insert = $this->db->insert($this->table, $data);
		if($insert){
			return true;
		}
	}

}
