<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImportModel extends CI_Model {

  protected $table_user;

  function __construct(){
      parent::__construct();
      $this->table_user = 'users';
  }

	public function insert_user($data){
		$insert = $this->db->insert_batch($this->table_user, $data);
		if($insert){
			return true;
		}
	}
	public function getDataUser(){
		$this->db->select('*');
		return $this->db->get($this->table_user)->result_array();
	}

}
