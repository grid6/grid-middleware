<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReturnCall extends CI_Controller
{

			function __construct()
			{
						parent::__construct();
		        // $this->auth();
						$this->mqdb = $this->load->database('mqtt', TRUE);
						$produrl="https://middle-xmqtt.gridfitnesshub.com";
						$mqtprod="https://rest-xmqtt.gridfitnesshub.com/";
						$mqtdev="https://dev-rest-xmqtt.gridfitnesshub.com/";
						$this->baze = (getenv("BASE_URL") == $produrl) ? $mqtprod : $mqtdev;
			}

			/*Function to set JSON output*/
			public function output($Return=array()){
				/*Set response header*/
				header("Access-Control-Allow-Origin: *");
				header("Content-Type: application/json; charset=UTF-8");
				/*Final JSON response*/
				exit(json_encode($Return));
			}

			public function index(){
					$success = false;
					$resp_code = 0;
					$resp_message = "";
					$data_output = $log_db = [];
					$headers = $this->input->get_request_header('key');
					if (empty($headers)) {
						$responses = [
								"success"=> false,
								"response_code"=> 401,
								"message" => "unauthorized",
								"data" => []
						];
						$this->output($responses);
					}
					$resp = json_decode(file_get_contents('php://input'));
					if(isset($resp->activity)){
							$whd=['machine_id'=>$resp->data->info->facesluiceId];
							$devis = $this->mqdb->get_where('devices', $whd);
							if($resp->activity == "verify" && $devis && $devis->num_rows() > 0){
								$derv = $devis->row();
								$uidx = (!empty(trim($resp->data->info->customId))) ? $resp->data->info->customId : 0;
								if(isset($derv->type) && $derv->type == 'out'){
									$success = true;
									$resp_message = "see you again";
									$log_db = [
											'user_id'=>(int)$uidx,
											'device_id'=>$derv->id,
											'activity'=>'out',
											'type'=>'out',
											'command_id'=>0,
											'request_data'=>json_encode($resp),
											'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
											'status'=>1
									];
									$this->db->insert("loggate", $log_db);
								}else if(isset($derv->type) && $derv->type == 'in'){
									if(!empty($uidx)){
											$usersql = $this->db->get_where("users", ["id"=>$uidx]);
											if($usersql && $usersql->num_rows() > 0){
													$whr1 = "user_id = '$uidx' AND ends_at > NOW()";
													$psql = $this->db->get_where("plan_subscriptions", $whr1);
													$creds = $isplan = false;
													$dains = $data_wallet = $actdata = [];
													$balance = 0;
													$lq= $actype="";
													if($psql && $psql->num_rows() > 0){
															$dains = $psql->row_array();
															$isplan = true;
															$whr3 = "user_id = '$uidx' AND DATE(schedule_date) = DATE(NOW()) AND attended_at IS NULL";
															$csql = $this->db->get_where("classes_joined", $whr3);
															if($csql && $csql->num_rows() > 0){
																	$creds=true;
																	$actype = "class-booking";
																	$dains = $csql->row_array();
																	$actdata = $csql->row_array();
																	$this->db->update('classes_joined', ['attended_at'=>date("Y-m-d H:i:s")], ['id'=>$csql->row()->id]);
															} else {
																$whr2 = "user_id = '$uidx' AND DATE(schedule_date) = DATE(NOW()) AND attended_at IS NULL";
																$tsql = $this->db->get_where("trainer_joined", $whr2);
																if($tsql && $tsql->num_rows() > 0){
																		$creds=true;
																		$dains = $tsql->row_array();
																		$actdata = $dains;
																		$actype="trainer-booking";
																		$this->db->update('trainer_joined', ['attended_at'=>date("Y-m-d H:i:s")], ['id'=>$tsql->row()->id]);
																}else {
																		$creds = true;
																		$actype = "membership-entrance";
																		$actdata = $dains;
																}
															}
													}else{
															$whr2 = "user_id = '$uidx' AND DATE(schedule_date) = DATE(NOW()) AND attended_at IS NULL";
															$tsql = $this->db->get_where("trainer_joined", $whr2);
															if($tsql && $tsql->num_rows() > 0){
																	$creds=true;
																	$dains = $tsql->row_array();
																	$actdata = $dains;
																	$actype="trainer-booking";
																	$this->db->update('trainer_joined', ['attended_at'=>date("Y-m-d H:i:s")], ['id'=>$tsql->row()->id]);
															}else{
																$whr3 = "user_id = '$uidx' AND DATE(schedule_date) = DATE(NOW()) AND attended_at IS NULL";
																$csql = $this->db->get_where("classes_joined", $whr3);
																if($csql && $csql->num_rows() > 0){
																	$creds=true;
																	$actype = "class-booking";
																	$dains = $csql->row_array();
																	$actdata = $csql->row_array();
																	$this->db->update('classes_joined', ['attended_at'=>date("Y-m-d H:i:s")], ['id'=>$csql->row()->id]);
																}else{
																	$credin = 1; /* change later */
																	$actype = "facility";
																	$whaw = ['holder_type'=>'App\Model\User', 'holder_id'=>$uidx];
																	$wsql = $this->db->get_where("wallets", $whaw);
																	if($wsql && $wsql->num_rows() > 0){
																			$wrow = $wsql->row();
																			$data_wallet = $wrow;
																			$balance=$wrow->balance;
																			if($wrow->balance < 10 && $wrow->expired_at <= date("Y-m-d H:i:s")){
																				$creds = false;
																			}else if($wrow->balance < 1){
																				$creds = false;
																			}else{
																				$creds = true;
																				$balance = $wrow->balance - $credin;
																				$dains = [
																						'payable_type' => 'App\Model\User',
																						'wallet_id' => $wrow->id,
																						'payable_id' => $uidx,
																						'type'	=> 'withdraw',
																						'amount' => $credin,
																						'balance' => $balance,
																						'confirmed' => 1,
																						'meta' => json_encode(['title'=>'Gate in', 'description'=>'Cost of Entrance Gateway']),
																						'uuid' => get_uuid()
																				];
																				$actdata = $dains;
																				if($this->db->insert('transactions', $dains)){
																						$this->db->query("UPDATE wallets SET balance = balance - $credin WHERE id = '".$wrow->id."'");
																				}
																			}
																	}else{
																			$creds = false;
																	}
																}
															}
													}
													/* check if can open gate */
													if($creds){
															$success = true;
															$data_mqtt = ['devices'=>[$derv->id]];
															$poz = json_encode($data_mqtt);
															$apimqtt = curl_api($this->baze."api/open-door", $poz, "POST");
										          $headers=array();
										          $data=explode("\n", $apimqtt);
										          array_shift($data);
										          foreach($data as $part){
										              $middle=explode(":",$part);
										              error_reporting(0);
										              $headers[trim($middle[0])] = trim($middle[1]);
										          }
															$resval = (array)json_decode(end($data), true);
															$resp_message = "welcome";
															if(!$isplan) $resp_message.= ", your balance is $balance";
															// unset($resp->info->pic);
															$data_output = [
																	'response' => $resval,
																	'request' => $resp
															];
															if($isplan) $data_output['subscribe'] = $psql->row();
															$log_db = [
																	'user_id'=>$uidx,
																	'device_id'=>$derv->id,
																	'activity'=>$actype,
																	'type'=>'in',
																	'command_id'=>0,
																	'request_data'=>json_encode($dains),
																	'response_data'=>json_encode($data_output),
																	'status'=>1
															];
															if(!empty($actdata)) $log_db['activity_data'] = json_encode($actdata);
															$this->db->insert("loggate", $log_db);
													}else if(!$creds && $isplan){
														$resp_code = 401;
														$resp_message = "unauthorized";
														$data_mqtt = ['customer_id'=>$uidx ,'devices'=>[$derv->id]];
														$poz2 = json_encode($data_mqtt);
														$rem = curl_api($this->baze."api/remove", $poz2, "POST");
														$headerz=array();
														$datm=explode("\n", $rem);
														array_shift($datm);
														foreach($datm as $partz){
																$middlez=explode(":",$partz);
																error_reporting(0);
																$headerz[trim($middlez[0])] = trim($middlez[1]);
														}
														$resval2 = (array)json_decode(end($datm), true);
														$resp_message = "user has been removed from device";
														$data_output = [
																'response' => $resval2,
																'request' => $resp
														];
														$log_db = [
																'user_id'=>$uidx,
																'device_id'=>$derv->id,
																'activity'=>$actype,
																'type' => 'in',
																'command_id'=>0,
																'request_data'=>json_encode($dains),
																'response_data'=>json_encode($data_output),
																'status'=>1
														];
														if(!empty($actdata)) $log_db['activity_data'] = json_encode($actdata);
														$this->db->insert("loggate", $log_db);
													}else{
														$resp_code = 401;
														$resp_message = "user doesn't have enough credit, balance = $balance";
														$data_output = $data_wallet;
														$log_db = [
																'user_id'=>$uidx,
																'device_id'=>$derv->id,
																'activity'=>$actype,
																'type' => 'in',
																'command_id'=>0,
																'request_data'=>json_encode($resp),
																'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
																'status'=>2
														];
														if(!empty($actdata)) $log_db['activity_data'] = json_encode($actdata);
														$this->db->insert("loggate", $log_db);
													}
												}else{
														$resp_code = 404;
														$resp_message = "user not found";
														$log_db = [
																'user_id'=>0,
																'device_id'=>$derv->id,
																'activity'=>'konnichiwa',
																'command_id'=>0,
																'type' => 'in',
																'request_data'=>json_encode($resp),
																'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
																'status'=>2
														];
														$this->db->insert("loggate", $log_db);
												}
										}else{
												$resp_code = 400;
												$resp_message = "user not enrolled on this device";
												$log_db = [
														'user_id'=>0,
														'device_id'=>$derv->id,
														'activity'=>'rocknotroll',
														'type'=>'in',
														'command_id'=>0,
														'request_data'=>json_encode($resp),
														'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
														'status'=>2
												];
												$this->db->insert("loggate", $log_db);
										}
									}else{
											if(isset($resp->device->type) && $resp->device->type != "front"){
													$resp_code = 200;
													$resp_message = $resp->activity." callback";
													$log_db = [
															'user_id'=>0,
															'device_id'=>$derv->id,
															'activity'=>(isset($resp->activity) && !empty($resp->activity)) ? $resp->activity : 'stranger',
															'type'=>'front',
															'command_id'=>0,
															'request_data'=>json_encode($resp),
															'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
															'status'=>2
													];
													$this->db->insert("loggate", $log_db);
											}
									}
							}else{
								$resp_message = (isset($resp->activity)) ? "callback ".$resp->activity: "device not found";
								if(isset($resp->device->type) && $resp->device->type =="front" && !empty($resp->data->info->pic)){
									$str = [
										'pic'=>stripslashes($resp->data->info->pic),
										'device_id'=>(isset($resp->device->id)) ? $resp->device->id : 0,
									];
									$this->db->insert('stranger_logs', $str);
								}
								$respdata = ['code'=>$resp_code,'message'=>$resp_message];
								if(isset($resp->data->info->SnapID) && !empty($resp->data->info->SnapID)){
									$data_mqtt = ['device'=>$resp->device->id, 'snap_id'=>$resp->data->info->SnapID];
									$poz = json_encode($data_mqtt);
									$apimqtt = curl_api($this->baze."api/snap-ack", $poz, "POST");
									$headers=array();
									$data=explode("\n", $apimqtt);
									array_shift($data);
									foreach($data as $part){
											$middle=explode(":",$part);
											error_reporting(0);
											$headers[trim($middle[0])] = trim($middle[1]);
									}
									$respdata['request_snap'] = $data_mqtt;
									$respdata['response_snap'] = (array)json_decode(end($data), true);
								}
								$resp_code = (isset($resp->activity)) ? 0 : 404;
								if(isset($resp->device->type) && $resp->device->type !="front"){
										$log_db = [
												'user_id'=>0,
												'device_id'=>(isset($resp->device->id)) ? $resp->device->id : 0,
												'activity'=>$resp->activity,
												'type'=>(isset($resp->device->type)) ? $resp->device->type : 'front',
												'command_id'=>0,
												'request_data'=>json_encode($resp),
												'response_data'=>json_encode($respdata),
												'status'=>1
										];
										$this->db->insert("loggate", $log_db);
								}
							}
		      }else{
							$resp_code = 400;
							$resp_message = "invalid response from mqtt";
							$data_output = $resp;
							$log_db = [
									'user_id'=>0,
									'device_id'=>(isset($resp->device->id)) ? $resp->device->id : 0,
									'activity'=>(isset($resp->activity)) ? "callback ".$resp->activity : "activity_not_found",
									'type'=>(isset($resp->device->type)) ? $resp->device->type : 'front',
									'command_id'=>0,
									'request_data'=>json_encode($resp),
									'response_data'=>json_encode(['code'=>$resp_code,'message'=>$resp_message]),
									'status'=>2
							];
							$this->db->insert("loggate", $log_db);
					}

					$responses = [
							"success"=> $success,
							"response_code"=> $resp_code,
							"message" => $resp_message,
							"data" => $data_output,
					];

					$this->output($responses);
			}
}

/* End of file ReturnCall.php */
