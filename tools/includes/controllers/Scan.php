<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

require_once(APPPATH.'libraries/Phpmqtt.php');


class Scan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("form");
    }

    public function face(){
      $baze = "https://dev-rest-xmqtt.gridfitnesshub.com/";
      $server = "xmqtt.gridfitnesshub.com";
      if(getenv("BASE_URL") == "https://middle-xmqtt.gridfitnesshub.com"){
        $baze = "https://rest-xmqtt.gridfitnesshub.com/";
      }
      $port = 1883;                     // change if necessary
      $username = 'smartship';                   // set your username
      $password = 'smartship123';                   // set your password
      $client_id = 'mqttx_fc2853aag'; // make sure this is unique for connecting to sever - you could use uniqid()

      $mqtt = new Bluerhinos\Phpmqtt($server, $port, $client_id);
      if(!$mqtt->connect(true, NULL, $username, $password)) {
      	echo "device not connected";
        exit(1);
      }

      $topics = 'mqtt/face/1787193/Rec';
      $json = $mqtt->subscribeAndWaitForMessage($topics, 0);
      $resp = json_decode($json);
      if(isset($resp->info)){

          $data_mqtt = [
            'devices'=>[1787193]
          ];
          $apimqtt = curl_api($baze."api/open-door", $data_mqtt);
          $headers=array();
          $data=explode("\n",$apimqtt);
          array_shift($data);
          foreach($data as $part){
              $middle=explode(":",$part);
              error_reporting(0);
              $headers[trim($middle[0])] = trim($middle[1]);
          }
          $resval = (array)json_decode(end($data), true);
          /* $content = [
            "operator" => "Unlock",
            "messageId" => (string)rand(1, 10),
            "info" => [
              "uid"=> (string)$resp->info->idCard,
              "openDoor"=>"1",
              "Showinfo"=> "please pass"
            ]
          ];
          $contentext = json_encode($content);
          $mqtt->publish($topics, $contentext, 0); */

          var_dump($resval);
      }
      $mqtt->close();
      // echo $json;
    }
}
